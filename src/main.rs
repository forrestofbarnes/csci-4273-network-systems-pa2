/* This file is based partly on the Rust Multi-Threaded Web Server tutorial:
 * https://doc.rust-lang.org/book/ch20-00-final-project-a-web-server.html
 * Retrieved 2020 March 3
 */
use std::{env, fs, str, thread};
use std::thread::{ThreadId, JoinHandle};
use std::sync::*; // Sending data between threads
use std::net::{TcpListener, TcpStream};
use std::io::ErrorKind;
use std::io::prelude::*; // read, write, and flush methods
use std::path::Path;
use std::time::Duration;
use std::ffi::OsStr; // For checking if a file exists
use regex::Regex;

type ThreadVec = Arc<Mutex<Vec<(ThreadId, JoinHandle<()>)>>>;
type CommPackage = (Arc<Mutex<()>>, mpsc::Sender<ThreadId>);

fn main() {
	// Create address port
	let args: Vec<String> = env::args().collect();
	let addrport = format!("{}{}", "127.0.0.1:",
		if args.len() <= 1 {
			"8888"
		}
		else {
			&args[1]
		}
	);
	let listener = TcpListener::bind(addrport).unwrap();
	
	// Create communication channels
	
	// Vector of all existing threads, in format (tid, threadhandle),
	// shared between the thread spawner and the thread joiner
	let threads = Arc::new(Mutex::new(Vec::new()));
	let threads2 = threads.clone();
	
	// Communication channel between all threads and thread_joiner
	// ensuring thread_joiner does not have to busy-wait for tids
	// of threads to be joined.
	let (tx_ready_join, rx_ready_join) = mpsc::channel();
	
	// We don't care about the handle for the thread_spawner thread because
	// it will never get joined until the server stops. The server stops
	// only when it receives a SIGINT, which kills all threads anyway.
	thread::spawn(move || { // Yes I'm spawning a spawner
		thread_spawner(listener, threads, tx_ready_join);
	});
	thread_joiner(threads2, rx_ready_join);
}

// Spawns a new thread for each incoming connection
fn thread_spawner(listener: TcpListener, threads: ThreadVec, tx_ready_join: mpsc::Sender<ThreadId>) {
	// Syntax sugar. This is an infinite loop that continuously gets new streams.
	for stream in listener.incoming() { 
		let stream = stream.unwrap();
		let (tx_id, rx_id) = mpsc::channel();
		
		let pushed = Arc::new(Mutex::new(()));
		let comm_package = (pushed.clone(), tx_ready_join.clone());
		let pushed = pushed.lock().unwrap();
		
		let handle = thread::spawn(move || {
			// Tell thread_spawner what the tid of this thread is
			tx_id.send(thread::current().id()).unwrap();
			
			// THIS IS THE IMPORTANT PART
			handle_connection(stream, comm_package);
		});
		
		// Receive tid from the spawned thread
		let tid = rx_id.recv().unwrap();
		// Send tid and handler of the new thread to thread_joiner.
		threads.lock().unwrap().push((tid, handle));
		// Ensure thread_joiner is given the "remove this thread" command
		// in the send_done function AFTER being told the thread exists.
		// This ensures no threads are left un-joined.
		drop(pushed);
	}
}

fn handle_connection(mut stream: TcpStream, mut comm_package: CommPackage) {
	// Set the keepalive timeout to 11 seconds.
	let keep_alive_timeout = Duration::from_secs(11);
	catch_error(stream.set_read_timeout(Some(keep_alive_timeout)), &mut stream,
		&mut comm_package);
	let mut keep_alive = true;
	while keep_alive {
		// Don't keep alive unless we find a keepalive request later
		keep_alive = false;
		let mut buffer = [0; 512];
		
		// Read from the TCPStream into the buffer
		match stream.read(&mut buffer) {
			Ok(_) => (),
			// If we had an earlier keepalive request, but the client didn't respond
			// in time, we will get a WouldBlock or TimedOut error. In that case,
			// just kill the thread with no error.
			Err(x) if x.kind() == ErrorKind::WouldBlock => break, // Linux error
			Err(x) if x.kind() == ErrorKind::TimedOut => break, // Windows error
			// Otherwise, something went wrong, send that error up the chain.
			Err(x) => catch_error(Err(x), &mut stream, &mut comm_package),
		}
		
		let strbuf = String::from_utf8_lossy(&buffer[..]);
		
		let packet: Vec<&str> = strbuf.split("\r\n").collect();
		
		let request: Vec<&str> = get_elem(&packet, 0, &mut comm_package)
			.split_whitespace().collect();
		
		let mut postdata = String::from("");
		for i in 1..packet.len() {
			let elems: Vec<&str> = packet[i].split_whitespace().collect();
			if elems.is_empty() {
				if get_elem(&request, 0, &mut comm_package) == "POST" {
					get_elem(packet, i+1, &mut comm_package); // Ensure element i+1 exists
					postdata = format!("<html><body><pre><h1>{}</h1></pre>", 
						packet[i+1..packet.len()].join("\r\n").trim_end_matches('\u{0}')
					);
				}
				break;
			}
			// If we find "keep-alive" after "Connection:", keep the connection alive
			if get_elem(&elems, 0, &mut comm_package) == "Connection:" {
				keep_alive = Regex::new(r"[Kk]eep-?[Aa]live").unwrap().is_match(elems[1]);
			}
		}
		
		// Find the www directory
		let mut root_dir = String::new();
		for (key, value) in env::vars() {
			if key == "CARGO_MANIFEST_DIR" {
				root_dir = value;
			}
		}
		let root_dir = format!("{}/www", root_dir);
		let defaultfile = "/index.html";
		let errorfile = "/404.html";
		let okreturn = "200 OK";
		let errorreturn = "404 NOT FOUND";
		
		// Currently only support GET and POST. Other functionality later.
		if let "GET" | "POST" = get_elem(&request, 0, &mut comm_package) {
			// Find the location of the requested file
			let mut filepath = format!("{}{}", root_dir,
				match get_elem(&request, 1, &mut comm_package) {
					"/" => defaultfile,
					x => x,
				}
			);
			// If it doesn't exist, return a 404 error
			let status = if Path::new(&filepath).exists() {
				okreturn
			}
			else {
				filepath = format!("{}{}", root_dir, errorfile);
				errorreturn
			};
			
			// Open the file
			let mut content = match fs::read(&filepath) {
				Ok(x) => x,
				// If the 404 path doesn't exist, send nothing.
				Err(_) => Vec::new(),
			};
			let mut response = format!(
				"{} {}\r\nContent-Type: {}\r\nContent-Length: {}\r\n\r\n{}",
				// request[2] will be HTTP/1.1
				get_elem(&request, 2, &mut comm_package),
				status,
				match Path::new(&filepath).extension().unwrap_or(OsStr::new(""))
						.to_str().unwrap_or("") {
					"html" => "text/html",
					"txt" => "text/plain",
					"png" => "image/png",
					"gif" => "image/gif",
					"jpg" => "image/jpg",
					"css" => "text/css",
					"js" => "application/javascript",
					_ => "text",
				},
				postdata.len() + content.len(),
				postdata
			).into_bytes();
			// Append the content to the header
			response.append(&mut content);
			// Send the content and flush the stream
			catch_error(stream.write(response.as_slice()), &mut stream, &mut comm_package);
			catch_error(stream.flush(), &mut stream, &mut comm_package);
		}
	}
	// Tell thread_joiner this thread is ready to be joined.
	send_done(&mut comm_package);
}

/* Catches errors early, which allows us to send the proper response
 * to the client before panicking.
 */
fn catch_error<T, U: std::fmt::Display + std::fmt::Debug>(problem: Result<T, U>,
		stream: &mut TcpStream, mut comm_package: &mut CommPackage) -> T {
	match problem {
		Ok(x) => x, // No problem, just unwrap the data and return it
		Err(x) => { // Oh shit
			// Tell the client we got an error
			let error_msg = "HTTP/1.1 500 Internal Server Error";
			stream.write(error_msg.as_bytes()).unwrap_or(0);
			// Tell thread_joiner this thread is ready to be joined.
			send_done(&mut comm_package);
			// Panic. Don't worry, this will get caught before it kills the server.
			panic!("{:?} received error {:?}: {}", thread::current().id(), x, x);
		},
	}
}

// Tells thread_joiner the current thread is ready to be joined.
fn send_done(comm_package: &mut CommPackage) {
	let (pushed, tx_ready_join) = comm_package;
	// Ensure thread_joiner is given the "remove this thread" command
	// AFTER being told the thread exists in thread_spawner
	let _pushed = pushed.lock().unwrap();
	// Tell thread_joiner this thread is ready to be joined
	tx_ready_join.send(thread::current().id()).unwrap();
}

/* Ensures that any segfaults (especially ones caused by a malformed packet)
 * don't cause threads to stick around un-joined.
 */
fn get_elem<'a>(str_arr: &Vec<&'a str>, index: usize,
		mut comm_package: &mut CommPackage) -> &'a str {
	if str_arr.len() <= index {
		// If the packet is malformed, tell thread_joiner to join the thread
		send_done(&mut comm_package);
	}
	// Let the error occur, panicking the thread and allowing it to be joined.
	str_arr[index]
}

// Joins all threads that are done with their work
fn thread_joiner(threads: ThreadVec, rx_ready_join: mpsc::Receiver<ThreadId>) {
	loop {
		// Wait for something to be ready to be joined
		let tid = rx_ready_join.recv().unwrap();
		// Acquire lock on list of threads
		let mut threads = threads.lock().unwrap();
		// Search for thread to be joined
		for i in 0..threads.len() {
			if threads[i].0 == tid {
				// Join thread to be joined
				if let Err(_) = threads.remove(i).1.join() {
					// If there was an error, don't let the panic spread.
				}
				// Move on to next thread that is ready to be joined.
				break;
			}
		}
		if cfg!(debug_assertions) {
			println!("Threads still working: {}", threads.len());
		}
	}
}